import { createRouter, createWebHistory } from 'vue-router'
import home from '../views/home.vue'
import product from "@/views/product";
import productEdit from "@/components/productEdit";
import productAdd from "@/components/productAdd";

const routes = [
  {
    path: '/',
    name: 'home',
    component: home
  },
  {
    path: '/goods',
    name: 'goods',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/goods.vue')
  },
  {
    path: '/goods/create',
    name: 'create',
    component: productAdd
  },
  {
    path: '/goods/:id',
    name: 'product',
    component: product
  },
  {
    path: '/goods/:id/edit',
    name: 'edit',
    component: productEdit
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
