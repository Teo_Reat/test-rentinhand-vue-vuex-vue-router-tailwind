import {createStore} from 'vuex'

export default createStore({
    state: {
        goods: [
            {
                id: 0,
                name: 'nirvana ',
                categories: 'some',
                price: 100, quantity: 5,
                images: [
                    'https://picsum.photos/200/150?random=1',
                    'https://picsum.photos/200/150?random=11',
                    'https://picsum.photos/200/150?random=111',
                ],
                description: 'Meaningless places is not the closest zen of the lotus. Contact and you will be shaped wonderfully.'
            },
            {
                id: 1,
                name: 'attitude ',
                categories: 'some',
                price: 100,
                quantity: 5,
                images: [
                    'https://picsum.photos/200/150?random=2',
                    'https://picsum.photos/200/150?random=22',
                    'https://picsum.photos/200/150?random=222',
                ],
                description: 'As i have hurted you, so you must desire one another. When one absorbs result and fear, one is able to witness freedom.'
            },
            {
                id: 2,
                name: 'ascension ',
                categories: 'some',
                price: 100,
                quantity: 5,
                images: [
                    'https://picsum.photos/200/150?random=3',
                    'https://picsum.photos/200/150?random=33',
                    'https://picsum.photos/200/150?random=333',
                ],
                description: 'Not zion or upstairs, develop the dogma. Confucius says: in the material world all explosion of the acceptances forget awareness.'
            },
            {
                id: 3,
                name: 'upstairs ',
                categories: 'another',
                price: 100,
                quantity: 5,
                images: [
                    'https://picsum.photos/200/150?random=4',
                    'https://picsum.photos/200/150?random=44',
                    'https://picsum.photos/200/150?random=444',
                ],
                description: 'A psychic form of acceptance is the control. Who can need the chaos and fear of a scholar if he has the psychic anger of the lotus?'
            },
            {
                id: 4,
                name: 'paradox ',
                categories: 'another',
                price: 100,
                quantity: 5,
                images: [
                    'https://picsum.photos/200/150?random=5',
                    'https://picsum.photos/200/150?random=55',
                    'https://picsum.photos/200/150?random=555',
                ],
                description: 'Our private man for issue is to remember others spiritually. A secret form of trust is the history.'
            },
            {
                id: 5,
                name: 'satori ',
                categories: 'another',
                price: 100,
                quantity: 5,
                images: [
                    'https://picsum.photos/200/150?random=6',
                    'https://picsum.photos/200/150?random=66',
                    'https://picsum.photos/200/150?random=666',
                ],
                description: 'Anger is not beloved in shangri-la, the great unknown, or space, but everywhere. Heavens is not the bright control of the aspect.'
            },
            {
                id: 6,
                name: 'lover  ',
                categories: 'another',
                price: 100,
                quantity: 5,
                images: [
                    'https://picsum.photos/200/150?random=61',
                    'https://picsum.photos/200/150?random=661',
                    'https://picsum.photos/200/150?random=6661',
                ],
                description: 'Be unbiased for whoever converts, because each has been gained with bliss.Futility happens when you develop zen so spiritually that whatsoever you are sitting is your manifestation.'
            },
            {
                id: 7,
                name: 'lord  ',
                categories: 'another',
                price: 100,
                quantity: 5,
                images: [
                    'https://picsum.photos/200/150?random=7',
                    'https://picsum.photos/200/150?random=77',
                    'https://picsum.photos/200/150?random=777',
                ],
                description: 'Always daily visualize the prime doer. When the individual of stigma illuminates the solitudes of the lord, the advice will know teacher.'
            },
            {
                id: 8,
                name: 'thought  ',
                categories: 'another',
                price: 100,
                quantity: 5,
                images: [
                    'https://picsum.photos/200/150?random=8',
                    'https://picsum.photos/200/150?random=88',
                    'https://picsum.photos/200/150?random=888',
                ],
                description: 'Man doesn’t oddly trap any creator — but the spirit is what empowers. Never capture the yogi, for you cannot handle it.'
            },
            {
                id: 9,
                name: 'hell  ',
                categories: 'another',
                price: 100,
                quantity: 5,
                images: [
                    'https://picsum.photos/200/150?random=9',
                    'https://picsum.photos/200/150?random=99',
                    'https://picsum.photos/200/150?random=999',
                ],
                description: 'One must hurt the lord in order to need the source of shining moonlight. The sinner respects thought which is not powerful.'
            },
            {
                id: 10,
                name: 'justice  ',
                categories: 'another',
                price: 100,
                quantity: 5,
                images: [
                    'https://picsum.photos/200/150?random=10',
                    'https://picsum.photos/200/150?random=1010',
                    'https://picsum.photos/200/150?random=101010',
                ],
                description: 'Earth of justice will purely praise a strange individual. Freedom doesn’t theosophically facilitate any spirit — but the individual is what listens.'
            },
        ]
    },
    mutations: {
        addProduct(state, product) {
            state.goods.push(product)
        },

        editProduct(state, product) {
            state.goods.name = product.name
            state.goods.price = product.price
        },

        deleteProduct(state, name) {
            const index = state.goods.findIndex(p => p.name === name)
            console.log(index)
            state.goods.splice(index, 1)
        }
    },
    actions: {},
    getters: {
        getGoods: state => state.goods
    },
    modules: {}
})
