module.exports = {
  purge: [
    './public/**/*.html',
    './src/**/*.html',
    './src/**/*.vue',
  ],
  darkMode: false,
  theme: {
    container: {
      center: true,
    },
    screens: {
      'sm': '480px',
      'md': '720px',
      'lg': '960px'
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [
    function ({addComponents}) {
      addComponents({
        '.container': {
          maxWidth: '100%',
          padding: '0 10px',
          '@screen sm': {
            maxWidth: '480px',
            padding: '0 15px',
          },
          '@screen md': {
            maxWidth: '720px'
          },
          '@screen lg': {
            maxWidth: '960px'
          }
        }
      })
    }
  ],
}
