module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
        ? '/somepath'
        : '/',
    lintOnSave: process.env.NODE_ENV !== 'production',
    productionSourceMap: false
}